### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### 工具
```markdown
jdk21
mysql8
idea2023.3.3
```
### 技术架构
```markdown
node21.6.1
vue3
Pinia
Vue-Router
Vite
Element Plus
SpringBoot
jwt、token
Spring Security
Mybatis Plus
Css3
TypeScript
```

