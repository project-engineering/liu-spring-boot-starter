# credit-web

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```


需要使用npm install axios 命令安装axios
需要使用npm install element-plus --save 命令安装element-plus
需要使用npm install @element-plus/icons-vue 命令安装icons-vue
需要使用npm install sass -D 命令安装sass
需要使用npm install vue-router --save 命令安装vue-router
需要使用npm install vuex --save  命令安装vuex
需要使用npm install js-cookie --save  命令安装js-cookie
需要使用npm install qs --save  命令安装qs
需要使用npm install pinia  命令安装pinia
需要使用npm install typescript --save-dev 命令安装typescript