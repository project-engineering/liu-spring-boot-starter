package com.springboot.credit.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.core.config.GlobalConfig.DbConfig;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.springboot.credit.handler.CommonMetaObjectHandler;
import com.zaxxer.hikari.HikariDataSource;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import java.io.IOException;

/**
 * <p>
 * MybatisPlus配置
 * </p>
 *
 * @author liuc
 * @since 2023-12-14
 */
@Configuration
@EnableTransactionManagement
@Log4j2
public class MybatisPlusConfig {
    @Resource
    HikariDataSource dataSource;
    private static Long MAX_LIMIT = 1000L;

    /**
     * 添加分页插件
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));//如果配置多个插件,切记分页最后添加
        //interceptor.addInnerInterceptor(new PaginationInnerInterceptor()); 如果有多数据源可以不配具体类型 否则都建议配上具体的DbType
        return interceptor;
    }

    /**
     * 初始化SqlSessionFactory
     * @param
     * @return org.apache.ibatis.session.SqlSessionFactory
     * @author liuc
     * @date 2021/11/15 21:10
     * @throws
     */
    @Bean("sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory() {
        log.info("初始化SqlSessionFactory");
        try {
            MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
            sqlSessionFactory.setDataSource(dataSource);
            MybatisConfiguration configuration = new MybatisConfiguration();
            configuration.setJdbcTypeForNull(JdbcType.NULL);
            configuration.setMapUnderscoreToCamelCase(true);
            configuration.setCacheEnabled(false);
            // 配置打印sql语句
            sqlSessionFactory.setPlugins(new Interceptor[]{mybatisPlusInterceptor()});
            sqlSessionFactory.setConfiguration(configuration);
            sqlSessionFactory.setGlobalConfig(globalConfiguration());
            log.info("初始化SqlSessionFactory完成");
            return sqlSessionFactory.getObject();
        } catch (IOException e) {
            log.error("mybatis resolver mapper*xml is error", e);
            return null;
        } catch (Exception e) {
            log.error("mybatis sqlSessionFactoryBean create error", e);
            return null;
        }
    }

    /**
     * 创建全局配置
     * @param
     * @return com.baomidou.mybatisplus.core.config.GlobalConfig
     * @author liuc
     * @date 2021/11/15 21:09
     * @throws
     */
    @Bean
    public GlobalConfig globalConfiguration() {
        // 全局配置文件
        GlobalConfig globalConfig = new GlobalConfig();
        DbConfig dbConfig = new DbConfig();
        /**
         * 默认为自增
         * AUTO->`0`("数据库ID自增")
         * INPUT->`1`(用户输入ID")
         * ID_WORKER->`2`("全局唯一ID")
         * UUID->`3`("全局唯一ID")
         */
        dbConfig.setIdType(IdType.AUTO);
        globalConfig.setDbConfig(dbConfig);
        globalConfig.setMetaObjectHandler(new CommonMetaObjectHandler());
        return globalConfig;
    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

    /**
     * 乐观锁插件
     * @param
     * @return com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor
     * @author liuc
     * @date 2021/11/15 21:09
     * @throws
     */
    @Bean
    public OptimisticLockerInnerInterceptor optimisticLockerInterceptor() {
        return new OptimisticLockerInnerInterceptor();
    }

    /**
     *  配置事务管理
     * @param
     * @return org.springframework.jdbc.datasource.DataSourceTransactionManager
     * @author liuc
     * @date 2021/11/13 23:08
     * @throws
     */
    @Bean(name = "transactionManager")
    public DataSourceTransactionManager transactionManager() {
        log.info("初始化DataSourceTransactionManager");
        return new DataSourceTransactionManager(dataSource);
    }
}
