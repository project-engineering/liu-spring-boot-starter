package com.springboot.credit.service.system.menu;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.credit.entity.system.menu.SysMenu;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
public interface SysMenuService extends IService<SysMenu> {

}
