package com.springboot.credit.service.impl.system.menu;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.credit.entity.system.menu.SysMenu;
import com.springboot.credit.mapper.system.menu.SysMenuMapper;
import com.springboot.credit.service.system.menu.SysMenuService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

}
