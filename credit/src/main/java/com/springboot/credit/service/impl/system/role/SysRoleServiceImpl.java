package com.springboot.credit.service.impl.system.role;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.credit.UtilValidate;
import com.springboot.credit.dto.system.role.RoleSelectType;
import com.springboot.credit.entity.system.role.SysRole;
import com.springboot.credit.mapper.system.role.SysRoleMapper;
import com.springboot.credit.param.system.role.RoleParam;
import com.springboot.credit.service.system.role.SysRoleService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {
    @Resource
    SysRoleMapper mapper;

    @Override
    public IPage<SysRole> getList(RoleParam param) {
        //构造分页对象
        IPage<SysRole> page = new Page<>(param.getPageNo(),param.getPageSize());
        //构造查询条件
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>();
        if (UtilValidate.isNotEmpty(param.getRoleName())){
            queryWrapper.like("role_name",param.getRoleName());
        }
        //执行查询
        return mapper.selectPage(page,queryWrapper);
    }

    @Override
    public List<RoleSelectType> getRoleSelectList() {
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>();
        List<SysRole> list = mapper.selectList(queryWrapper);
        List<RoleSelectType> roleSelectTypeList = new ArrayList<>();
        if (UtilValidate.isNotEmpty(list)) {
            for (SysRole item : list) {
                RoleSelectType roleSelectType = RoleSelectType.builder()
                        .roleId(item.getRoleId())
                        .roleName(item.getRoleName())
                        .build();
                roleSelectTypeList.add(roleSelectType);
            }
        }
        return roleSelectTypeList;
    }

    @Override
    public boolean addRole(SysRole sysRole) {
        sysRole.setRoleId(IdUtil.simpleUUID());
        int i = mapper.insert(sysRole);
        if (i > 0) {
            return true;
        }else {
            return false;
        }
    }
}
