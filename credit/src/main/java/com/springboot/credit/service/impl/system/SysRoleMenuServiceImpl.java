package com.springboot.credit.service.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.credit.entity.system.SysRoleMenu;
import com.springboot.credit.mapper.system.SysRoleMenuMapper;
import com.springboot.credit.service.system.SysRoleMenuService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色菜单表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

}
