package com.springboot.credit.service.system.role;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.credit.dto.system.role.RoleSelectType;
import com.springboot.credit.entity.system.role.SysRole;
import com.springboot.credit.param.system.role.RoleParam;
import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
public interface SysRoleService extends IService<SysRole> {
    IPage<SysRole> getList(RoleParam param);

    List<RoleSelectType> getRoleSelectList();

    boolean addRole(SysRole sysRole);
}
