package com.springboot.credit.controller.system.menu;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 菜单表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@RestController
@RequestMapping("/api/menu")
public class SysMenuController {

}
