package com.springboot.credit.controller.system.user;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.springboot.credit.annotation.Log;
import com.springboot.credit.constants.Constants;
import com.springboot.credit.entity.system.user.SysUser;
import com.springboot.credit.param.system.user.UserParam;
import com.springboot.credit.service.system.user.SysUserService;
import com.springboot.credit.util.Result;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@RestController
@RequestMapping("/api/user")
public class SysUserController {
    @Resource
    private SysUserService sysUserService;

    /**
     * 新增用户信息
     * @param sysUser
     * @return
     */
    @Log(modul = "用户管理", type = Constants.ADD, desc = "新增用户信息")
    @PostMapping
    public Result add(@RequestBody SysUser sysUser){
        if (sysUserService.addUser(sysUser)) {
            return Result.success("新增用户成功!");
        }
        return Result.fail("新增用户失败!");
    }

    /**
     * 修改用户信息
     * @param sysUser
     * @return
     */
    @Log(modul = "用户管理", type = Constants.UPDATE, desc = "修改用户信息")
    @PutMapping
    public Result update(@RequestBody SysUser sysUser){
        if (sysUserService.updateUser(sysUser)) {
            return Result.success("修改用户成功!");
        }
        return Result.fail("修改用户失败!");
    }

    /**
     * 删除角色信息
     * @param userId
     * @return
     */
    @Log(modul = "用户管理", type = Constants.DELETE, desc = "删除角色信息")
    @DeleteMapping("/{userId}")
    public Result delete(@PathVariable("userId") String userId){
        if (sysUserService.deleteUser(userId)) {
            return Result.success("删除用户成功!");
        }
        return Result.fail("删除用户失败!");
    }

    /**
     * 分页查询用户信息
     * @param param
     * @return
     */
    @Log(modul = "用户管理", type = Constants.SELECT, desc = "分页查询用户信息")
    @GetMapping("/list")
    public Result list(UserParam param){
        IPage<SysUser> list = sysUserService.getList(param);
        return Result.success("查询成功",list);
    }

    @Log(modul = "用户管理", type = Constants.SELECT, desc = "重置密码")
    @PostMapping("/resetPwd/{userId}")
    public Result resetPwd(@PathVariable("userId") String userId){
        if (sysUserService.resetPwd(userId)>0) {
            return Result.success("重置密码成功!");
        }
        return Result.fail("重置密码失败!");
    }
}
