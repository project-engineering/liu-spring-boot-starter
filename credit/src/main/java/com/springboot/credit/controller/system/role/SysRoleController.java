package com.springboot.credit.controller.system.role;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.springboot.credit.annotation.Log;
import com.springboot.credit.constants.Constants;
import com.springboot.credit.dto.system.role.RoleSelectType;
import com.springboot.credit.entity.system.role.SysRole;
import com.springboot.credit.param.system.role.RoleParam;
import com.springboot.credit.service.system.role.SysRoleService;
import com.springboot.credit.util.Result;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@RestController
@RequestMapping("/api/role")
public class SysRoleController {
    @Resource
    private SysRoleService sysRoleService;

    /**
     * 新增角色信息
     * @param sysRole
     * @return
     */
    @Log(modul = "角色管理", type = Constants.ADD, desc = "新增角色信息")
    @PostMapping
    public Result add(@RequestBody SysRole sysRole){
        if (sysRoleService.addRole(sysRole)) {
            return Result.success("新增角色成功!");
        }
        return Result.fail("新增角色失败!");
    }

    /**
     * 修改角色信息
     * @param sysRole
     * @return
     */
    @Log(modul = "角色管理", type = Constants.UPDATE, desc = "修改角色信息")
    @PutMapping
    public Result update(@RequestBody SysRole sysRole){
        if (sysRoleService.updateById(sysRole)) {
            return Result.success("修改角色成功!");
        }
        return Result.fail("修改角色失败!");
    }

    /**
     * 删除角色信息
     * @param roleId
     * @return
     */
    @Log(modul = "角色管理", type = Constants.DELETE, desc = "删除角色信息")
    @DeleteMapping("/{roleId}")
    public Result delete(@PathVariable("roleId") Long roleId){
        if (sysRoleService.removeById(roleId)) {
            return Result.success("删除角色成功!");
        }
        return Result.fail("删除角色失败!");
    }

    /**
     * 分页查询角色信息
     * @param param
     * @return
     */
    @Log(modul = "角色管理", type = Constants.SELECT, desc = "分页查询角色信息")
    @GetMapping("/list")
    public Result list(RoleParam param){
        IPage<SysRole> list = sysRoleService.getList(param);
        return Result.success("查询成功",list);
    }

    /**
     * 查询角色下拉框信息
     * @return
     */
    @GetMapping("/getRoleSelectList")
    public Result getRoleSelectList(){
        List<RoleSelectType> list = sysRoleService.getRoleSelectList();
        return Result.success("查询成功",list);
    }
}
