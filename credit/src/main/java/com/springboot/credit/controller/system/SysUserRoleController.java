package com.springboot.credit.controller.system;

import com.springboot.credit.annotation.Log;
import com.springboot.credit.constants.Constants;
import com.springboot.credit.service.system.SysUserRoleService;
import com.springboot.credit.util.Result;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户角色表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@RestController
@RequestMapping("/api/userRole")
public class SysUserRoleController {
    @Resource
    SysUserRoleService sysUserRoleService;
    @Log(modul = "用户角色管理", type = Constants.SELECT, desc = "用户角色查询")
    @GetMapping("/getRoleByUserId/{userId}")
    public Result getRoleByUserId(@PathVariable("userId") String userId){
        return Result.success("用户角色查询成功!",sysUserRoleService.getRoleByUserId(userId));
    }

}
