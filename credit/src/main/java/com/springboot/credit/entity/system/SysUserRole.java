package com.springboot.credit.entity.system;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * <p>
 * 用户角色表
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Data
@Builder
@TableName("sys_user_role")
@ApiModel(value = "SysUserRole对象", description = "用户角色表")
public class SysUserRole {

    @ApiModelProperty("用户角色主键ID")
    @TableId(value = "user_role_id")
    private String userRoleId;

    @ApiModelProperty("用户ID")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty("角色ID")
    @TableField("role_id")
    private String roleId;
}
