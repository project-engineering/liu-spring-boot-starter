package com.springboot.credit.entity.system.menu;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 菜单表
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_menu")
@ApiModel(value = "SysMenu对象", description = "菜单表")
public class SysMenu {

    @ApiModelProperty("菜单主键ID")
    @TableId(value = "menu_id")
    private String menuId;

    @ApiModelProperty("父菜单ID")
    @TableField("parent_id")
    private String parentId;

    @ApiModelProperty("菜单名称")
    @TableField("title")
    private String title;

    @ApiModelProperty("权限字段")
    @TableField("`code`")
    private String code;

    @ApiModelProperty("路由name")
    @TableField("`name`")
    private String name;

    @ApiModelProperty("路由地址")
    @TableField("`path`")
    private String path;

    @ApiModelProperty("组件路径")
    @TableField("url")
    private String url;

    @ApiModelProperty("菜单类型(0-目录,1-菜单,2-按钮)")
    @TableField("`type`")
    private String type;

    @ApiModelProperty("菜单图标")
    @TableField("icon")
    private String icon;

    @ApiModelProperty("上级菜单名称")
    @TableField("parent_name")
    private String parentName;

    @ApiModelProperty("显示顺序")
    @TableField("order_num")
    private Integer orderNum;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;
}
