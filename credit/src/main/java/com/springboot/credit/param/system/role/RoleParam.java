package com.springboot.credit.param.system.role;

import com.springboot.credit.page.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleParam extends Page {
    @ApiModelProperty("角色名称")
    private String roleName;
}
