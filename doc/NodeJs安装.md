[TOC]

# 一、下载NodeJs
[NodeJs官网](https://nodejs.org/en)
![img.png](images/img.png)

![img.png](images/nodejs安装包.png)

# 二、安装NodeJs
运行上面下载的nodejs安装程序，出现欢迎的安装界面，选择next
![img.png](images/nodejs安装1.png)

勾选I accept the terms in the License Agreement

![img.png](images/nodejs安装2.png)

单击图中的Next，进入选择Node.js安装位置设置的界面

![img.png](images/nodejs安装3.png)

直接选择next
![img.png](images/nodejs安装4.png)

![img.png](images/nodejs安装5.png)

选中install
![img.png](images/nodejs安装6.png)

点击finish安装完成
![img.png](images/nodejs安装7.png)

至此，Node.js安装程序的安装实际已经完成。

# 三、验证NodeJs环境变量
NodeJs安装完毕后，会自动配置好环境变量，我们验证一下是否安装成功，通过：node -v

![img.png](images/验证NodeJs.png)

# 四、配置npm的全局安装路径
![img.png](images/配置npm.png)
使用管理员身份运行命令行，在命令行中，执行如下指令：
```markdown
npm config set prefix "D:\tools\nodejs"
```
注意：D:\tools\nodejs 这个目录是NodeJs的安装目录
![img.png](images/配置npm1.png)

# 五、更换安装包的源
设置
```markdown
npm config set registry http://registry.npm.taobao.org
```
![img.png](images/更换安装包的源设置.png)
检查
npm config get registry
![img.png](images/更换安装包的源检查.png)
