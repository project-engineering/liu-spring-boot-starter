package com.springboot.credit.lang;

/**
 * @author liuc
 */
public interface IsEmpty {
    boolean isEmpty();
}
