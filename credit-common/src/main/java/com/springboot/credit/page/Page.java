package com.springboot.credit.page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Api(description ="分页对象")
public class Page {

    @ApiModelProperty("起始页")
    private Integer pageNo;

    @ApiModelProperty("页大小")
    private Integer pageSize;
}
